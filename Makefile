.PHONY: all build install debug

FLATPAK=$(shell which flatpak)
FLATPAK_BUILDER=$(shell which flatpak-builder)

FLATPAK_MANIFEST=go-astielectron-flatpak-demo.yml
FLATPAK_APPID=com.gitlab.go-astilectron-flatpak-demo

all: install

build:
	@echo "Building Flatpak..."
	$(FLATPAK_BUILDER) build --verbose --force-clean $(FLATPAK_MANIFEST)

install:
	@echo "Building and installing Flatpak..."
	$(FLATPAK_BUILDER) build --verbose --force-clean --install --user $(FLATPAK_MANIFEST)

install-build-dependencies:
	$(FLATPAK) install org.freedesktop.Platform//21.08
	$(FLATPAK) install org.freedesktop.Sdk//21.08
	$(FLATPAK) install org.freedesktop.Sdk.Extension.golang//21.08
	$(FLATPAK) install org.electronjs.Electron2.BaseApp//21.08

debug:
	$(FLATPAK_BUILDER) --verbose --run build $(FLATPAK_MANIFEST) sh

debug-installed:
	$(FLATPAK) run --command=sh --devel $(FLATPAK_APPID)

run:
	$(FLATPAK) run $(FLATPAK_APPID)

uninstall:
	$(FLATPAK) uninstall --delete-data --assumeyes $(FLATPAK_APPID)
