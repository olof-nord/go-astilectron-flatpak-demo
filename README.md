# go-astilectron-flatpak-demo

Demo go-astilectron-demo app with Flatpak packaging.

https://github.com/asticode/go-astilectron-demo

## Build

To build the Flatpak, use the following:

`make build`

## Install

To install the Flatpak, use the following:

`make install`

## Debug

To debug (open a shell to it) the built Flatpak, use the following:

`make debug`

## astilectron bundler internals

When starting the bundle with `/app/bin/go-astilectron-demo`, it in its turn is executing the following.

* extracts the Electron binary to `/app/bin/vendor/electron-linux-amd64/electron`.
* extracts the go-astilectron-demo js/html/css bundle to `/app/bin/resources`.
* extracts the astilectron Electron app to `/app/bin/vendor/astilectron`.

The Electron startup command is `/app/bin/vendor/electron-linux-amd64/electron /app/bin/vendor/astilectron/main.js`.

## Electron with Wayland

Note that Electron needs two flags to startup correctly with
Wayland: `--enable-features=UseOzonePlatform --ozone-platform=wayland`.

Additionally, the following environment variables needs to be set.

```shell
export DISPLAY=:0
export WAYLAND_DISPLAY=wayland-0
```

Again, many thanks to [Ferenc-](https://github.com/Ferenc-) for finding this out: for the full investigation
see [this issue](https://github.com/nanu-c/axolotl/issues/468#issuecomment-944353279).

## astilectron with Flatpak read-only file system

During astilectron bootstrap, archives are unpacked in two child directories (/resources and /vendor) of the electron
bundle. These two directories needs to have read-write permissions.

The standard Flatpak install location, /app/bin, is read-only. To work around this, an environment variable is added to
the bootstrap of astilectron, which lets the location be specified at runtime.

Many thanks to Bartłomiej Piotrowski from the Flatpak dev email list for pointing out the Flatpak $XDG_DATA_HOME config
value.

And also many thanks to [asticode](https://github.com/asticode) for answering
my [question](https://github.com/asticode/go-astilectron-bundler/issues/100) regarding the startup configuration for
astilectron-bundler.

## Logs

Here is an example of a successful startup:

```log
2022/01/31 20:22:41 Starting cmd /app/bin/vendor/electron-linux-amd64/electron /app/bin/vendor/astilectron/main.js 127.0.0.1:38825 true --no-sandbox --enable-features=UseOzonePlatform --ozone-platform=wayland
2022/01/31 20:22:41 Astilectron says: {"name":"app.event.ready","targetID":"app","displays":{"all":[{"id":4,"bounds":{"x":1720,"y":0,"width":1920,"height":1080},"workArea":{"x":1720,"y":0,"width":1920,"height":1080},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":1920,"height":1080},"displayFrequency":0,"workAreaSize":{"width":1920,"height":1080},"scaleFactor":2,"rotation":0,"internal":false,"touchSupport":"unknown"},{"id":5,"bounds":{"x":0,"y":720,"width":3440,"height":1440},"workArea":{"x":0,"y":720,"width":3440,"height":1440},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":3440,"height":1440},"displayFrequency":0,"workAreaSize":{"width":3440,"height":1440},"scaleFactor":1,"rotation":0,"internal":false,"touchSupport":"unknown"}],"primary":{"id":4,"bounds":{"x":1720,"y":0,"width":1920,"height":1080},"workArea":{"x":1720,"y":0,"width":1920,"height":1080},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":1920,"height":1080},"displayFrequency":0,"workAreaSize":{"width":1920,"height":1080},"scaleFactor":2,"rotation":0,"internal":false,"touchSupport":"unknown"}},"supported":{"notification":true}}
2022/01/31 20:22:41 Sending to Astilectron: {"name":"window.cmd.create","targetID":"1","sessionId":"2","url":"file:///app/bin/resources/app/index.html","windowOptions":{"backgroundColor":"#333","center":true,"height":700,"icon":"/app/bin/resources/icon.png","title":"go-astilectron-demo","width":700}}
2022/01/31 20:22:41 Stderr says: [131:0131/202241.372870:ERROR:cursor_loader.cc(114)] Failed to load a platform cursor of type kNull
2022/01/31 20:22:41 Astilectron says: {"name":"window.event.move","targetID":"1"}
2022/01/31 20:22:41 Stderr says: [171:0131/202241.509203:ERROR:gpu_init.cc(457)] Passthrough is not supported, GL is egl, ANGLE is
2022/01/31 20:22:41 Stderr says: [171:0131/202241.513689:ERROR:sandbox_linux.cc(376)] InitializeSandbox() called with multiple threads in process gpu-process.
2022/01/31 20:22:41 Astilectron says: {"name":"window.event.focus","targetID":"1"}
2022/01/31 20:22:41 Stderr says: [131:0131/202241.638570:ERROR:cursor_loader.cc(114)] Failed to load a platform cursor of type kNull
2022/01/31 20:22:41 Astilectron says: {"name":"window.event.did.finish.load","targetID":"1"}
2022/01/31 20:22:41 Sending to Astilectron: {"name":"menu.cmd.create","targetID":"3","menu":{"id":"3","items":[{"id":"4","options":{"label":"File"},"rootId":"app","submenu":{"id":"5","items":[{"id":"6","options":{"label":"About"},"rootId":"app"},{"id":"7","options":{"role":"close"},"rootId":"app"}],"rootId":"app"}}],"rootId":"app"}}
2022/01/31 20:22:41 Astilectron says: {"name":"menu.event.created","targetID":"3"}
2022/01/31 20:22:41 astikit: worker is now waiting...
2022/01/31 20:22:41 Astilectron says: {"name":"window.event.message","targetID":"1","message":{"name":"explore"},"callbackId":"1"}
2022/01/31 20:22:41 Sending to Astilectron: {"name":"window.cmd.message.callback","targetID":"1","callbackId":"1","message":{"name":"explore.callback","payload":{"dirs":[{"name":"..","path":"/home"},{"name":"temp","path":"/home/olof/temp"}],"files":{"data":{"datasets":[{"backgroundColor":["rgba(255, 206, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(255, 99, 132, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)"],"borderColor":["rgba(255, 206, 86, 1)","rgba(75, 192, 192, 1)","rgba(255,99,132,1)","rgba(54, 162, 235, 1)","rgba(153, 102, 255, 1)"],"data":[476278,43719,42991,17730,17317]}],"labels":[".zsh_history",".zcompdump-pedro-5.8",".zcompdump",".viminfo","other"]},"type":"pie"},"files_count":23,"files_size":"584kb","path":"/home/olof"}}}
2022/01/31 20:22:41 Astilectron says: {"name":"window.event.ready.to.show","targetID":"1"}
2022/01/31 20:22:42 Astilectron says: {"name":"window.event.blur","targetID":"1"}
2022/01/31 20:22:46 Sending to Astilectron: {"name":"window.cmd.message","targetID":"1","message":{"name":"check.out.menu","payload":"Don't forget to check out the menu!"}}
```

Here is an example of a failed startup, note `context canceled` message at the end.

```log
2022/01/31 20:00:37 Starting cmd /app/bin/vendor/electron-linux-amd64/electron /app/bin/vendor/astilectron/main.js 127.0.0.1:33411 true --no-sandbox --enable-features=UseOzonePlatform --ozone-platform=wayland
2022/01/31 20:00:37 Astilectron says: {"name":"app.event.ready","targetID":"app","displays":{"all":[{"id":4,"bounds":{"x":1720,"y":0,"width":1920,"height":1080},"workArea":{"x":1720,"y":0,"width":1920,"height":1080},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":1920,"height":1080},"displayFrequency":0,"workAreaSize":{"width":1920,"height":1080},"scaleFactor":2,"rotation":0,"internal":false,"touchSupport":"unknown"},{"id":5,"bounds":{"x":0,"y":720,"width":3440,"height":1440},"workArea":{"x":0,"y":720,"width":3440,"height":1440},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":3440,"height":1440},"displayFrequency":0,"workAreaSize":{"width":3440,"height":1440},"scaleFactor":1,"rotation":0,"internal":false,"touchSupport":"unknown"}],"primary":{"id":4,"bounds":{"x":1720,"y":0,"width":1920,"height":1080},"workArea":{"x":1720,"y":0,"width":1920,"height":1080},"accelerometerSupport":"unknown","monochrome":false,"colorDepth":24,"colorSpace":"{primaries:BT709, transfer:IEC61966_2_1, matrix:RGB, range:FULL}","depthPerComponent":8,"size":{"width":1920,"height":1080},"displayFrequency":0,"workAreaSize":{"width":1920,"height":1080},"scaleFactor":2,"rotation":0,"internal":false,"touchSupport":"unknown"}},"supported":{"notification":true}}
2022/01/31 20:00:37 Sending to Astilectron: {"name":"window.cmd.create","targetID":"1","sessionId":"2","url":"file:///app/bin/resources/app/index.html","windowOptions":{"backgroundColor":"#333","center":true,"height":700,"icon":"/app/bin/resources/icon.png","title":"go-astilectron-demo","width":700}}
2022/01/31 20:00:37 Stderr says: [19:0131/200037.371094:ERROR:cursor_loader.cc(114)] Failed to load a platform cursor of type kNull
2022/01/31 20:00:37 Stderr says: [60:0100/000000.450282:ERROR:gpu_init.cc(457)] Passthrough is not supported, GL is egl, ANGLE is
2022/01/31 20:00:37 astikit: received signal child exited
2022/01/31 20:00:37 Stderr says: [60:0100/000000.941500:ERROR:sandbox_linux.cc(376)] InitializeSandbox() called with multiple threads in process gpu-process.
2022/01/31 20:00:37 '/app/bin/vendor/electron-linux-amd64/electron' exited with code: -1
2022/01/31 20:00:37 App has crashed
2022/01/31 20:00:37 Stopping...
2022/01/31 20:00:37 astikit: stopping worker...
2022/01/31 20:00:37 Closing...
2022/01/31 20:00:37 accept tcp 127.0.0.1:33411: use of closed network connection while TCP accepting
2022/01/31 20:00:37 Stopping...
2022/01/31 20:00:37 running bootstrap failed: creating menu failed: context canceled
```
